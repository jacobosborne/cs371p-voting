.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

FILES :=           \
    .gitignore     \
    voting-tests   \
    Voting.c++     \
    Voting.h       \
    makefile       \
    RunVoting.c++  \
    RunVoting.h    \
    RunVoting.in   \
    RunVoting.out  \
    TestVoting.c++ \
    voting-tests/jacobosborne-RunVoting.in  \
    voting-tests/jacobosborne-RunVoting.out \
    Voting.log     \
    html           \

voting-tests:
	git clone https://gitlab.com/gpdowning/cs371p-voting-tests.git voting-tests

html: Doxyfile Voting.h
	doxygen Doxyfile

Voting.log:
	git log > Voting.log

Doxyfile:
	doxygen -g

RunVoting: Voting.h Voting.c++ RunVoting.h RunVoting.c++
	-cppcheck Voting.c++
	-cppcheck RunVoting.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Voting.c++ RunVoting.c++ -o RunVoting

RunVoting.c++x: RunVoting
	./RunVoting < RunVoting.in > RunVoting.tmp
	-diff RunVoting.tmp RunVoting.out

TestVoting: Voting.h Voting.c++ TestVoting.c++
	-cppcheck Voting.c++
	-cppcheck TestVoting.c++
	g++ -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra Voting.c++ TestVoting.c++ -o TestVoting -lgtest -lgtest_main -pthread

TestVoting.c++x: TestVoting
	valgrind ./TestVoting
	gcov -b Voting.c++ | grep -A 5 "File '.*Voting.c++'"

all: RunVoting

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunVoting
	rm -f TestVoting

config:
	git config -l

ctd:
	checktestdata TestVoting.ctd RunVoting.in

docker:
	docker run -it -v $(PWD):/usr/voting -w /usr/voting gpdowning/gcc

format:
	astyle Voting.c++
	astyle Voting.h
	astyle RunVoting.h
	astyle RunVoting.c++
	astyle TestVoting.c++

init:
	touch README
	git init
	git remote add origin git@gitlab.comgpdowning/cs371p-voting.git
	git add README
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Voting.c++
	git add Voting.h
	-git add Voting.log
	-git add html
	git add makefile
	git add RunVoting.h
	git add RunVoting.c++
	git add RunVoting.in
	git add RunVoting.out
	git add Test.ctd
	git add TestVoting.c++
	git commit -m "another commit"
	git push
	git status

run: RunVoting.c++x TestVoting.c++x

scrub:
	make clean
	rm -f Voting.log
	rm -f Doxyfile
	rm -rf voting-tests
	rm -rf html
	rm -rf latex

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

versions:
	which astyle
	astyle --version
	@echo
	dpkg -s libboost-dev | grep 'Version'
	@echo
	ls -al /usr/lib/*.a
	@echo
	which checktestdata
	checktestdata --version
	@echo
	which cmake
	cmake --version
	@echo
	which cppcheck
	cppcheck --version
	@echo
	which doxygen
	doxygen --version
	@echo
	which g++
	g++ --version
	@echo
	which gcov
	gcov --version
	@echo
	which git
	git --version
	@echo
	which make
	make --version
	@echo
	which valgrind
	valgrind --version
	@echo
	which vim
	vim --version
