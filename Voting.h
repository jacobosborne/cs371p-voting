#ifndef VOTING
#define VOTING

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

#include <list>

#include "RunVoting.h"
#include "Voting.h"

struct VoteCase {
    int num_candidates;
    int ballot_count;
    vector< string > candidates;
    vector< vector<int> > ballots;

    VoteCase ( int n_cand, int ball_c, vector<string> cand, vector< vector<int> > ball ) :
        num_candidates( n_cand ), ballot_count( ball_c ), candidates( cand ), ballots( ball ) { };
};

list<VoteCase> parse_cases( istream& r, ostream& w );

VoteCase parse_case( istream& r, ostream& w );

list<int> evaluate_case( int num_candidates, vector<vector<int>>& ballots, int ballot_count );



#endif
