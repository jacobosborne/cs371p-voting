// --------------
// TestVoting.c++
// --------------

// --------
// includes
// --------

#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include "gtest/gtest.h"

#include "Voting.h"

// --------
// ReadCase
// --------

TEST( VotingFixture, readcase_simple ) {
    istringstream r( "1\nAlice\n1\n" );
    ostringstream w;
    const VoteCase vc = parse_case( r, w );

    ASSERT_EQ( vc.num_candidates, 1 );
    ASSERT_EQ( vc.ballot_count, 1 );
    ASSERT_EQ( vc.candidates, vector<string>( { "Alice" } ) );
    ASSERT_EQ( vc.ballots, vector<vector<int>>( { { 1 } } ) );
}

TEST( VotingFixture, readcase_complex ) {
    istringstream r( "3\nJohn Doe\nJane Smith\nSirhan Sirhan\n1 2 3\n2 1 3\n2 3 1\n1 2 3\n3 1 2\n" );
    ostringstream w;
    const VoteCase vc = parse_case( r, w );

    const vector<string> expected_candidates = { "John Doe", "Jane Smith", "Sirhan Sirhan" };
    const vector< vector<int> > expected_ballots = {
        { 1, 2, 3 },
        { 2, 1, 3 },
        { 2, 3, 1 },
        { 1, 2, 3 },
        { 3, 1, 2 }
    };
    ASSERT_EQ( vc.num_candidates, 3 );
    ASSERT_EQ( vc.ballot_count, 5 );
    ASSERT_EQ( vc.candidates, expected_candidates );
    ASSERT_EQ( vc.ballots, expected_ballots );
}

// ---------
// ReadCases
// ---------

TEST( VotingFixture, readcases_simple ) {
    istringstream r( "1\n\n1\nAlice\n1\n" );
    ostringstream w;
    const list<VoteCase> vcs = parse_cases( r, w );

    list<VoteCase>::const_iterator iter = vcs.begin();

    ASSERT_EQ( iter->num_candidates, 1 );
    ASSERT_EQ( iter->ballot_count, 1 );
    ASSERT_EQ( iter->candidates, vector<string>( { "Alice" } ) );
    ASSERT_EQ( iter->ballots, vector<vector<int>>( { { 1 } } ) );
}

TEST( VotingFixture, readcases_complex ) {
    istringstream r( "2\n\n1\nAlice\n1\n\n3\nJohn Doe\nJane Smith\nSirhan Sirhan\n1 2 3\n2 1 3\n2 3 1\n1 2 3\n3 1 2\n" );
    ostringstream w;
    const list<VoteCase> vcs = parse_cases( r, w );

    list<VoteCase>::const_iterator iter = vcs.begin();

    ASSERT_EQ( iter->num_candidates, 1 );
    ASSERT_EQ( iter->ballot_count, 1 );
    ASSERT_EQ( iter->candidates, vector<string>( { "Alice" } ) );
    ASSERT_EQ( iter->ballots, vector<vector<int>>( { { 1 } } ) );

    iter++;

    const vector<string> expected_candidates = { "John Doe", "Jane Smith", "Sirhan Sirhan" };
    const vector< vector<int> > expected_ballots = {
        { 1, 2, 3 },
        { 2, 1, 3 },
        { 2, 3, 1 },
        { 1, 2, 3 },
        { 3, 1, 2 }
    };
    ASSERT_EQ( iter->num_candidates, 3 );
    ASSERT_EQ( iter->ballot_count, 5 );
    ASSERT_EQ( iter->candidates, expected_candidates );
    ASSERT_EQ( iter->ballots, expected_ballots );
}

// ------
// Simple
// ------

TEST( VotingFixture, simple_1can1bal ) {
    const int num_candidates = 1;
    const int num_ballots = 1;
    vector<vector<int>> ballots = {
        { 1 }
    };

    const list<int> expected_result = { 0 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}

TEST( VotingFixture, simple_5can1bal ) {
    const int num_candidates = 5;
    const int num_ballots = 1;
    vector<vector<int>> ballots = {
        { 1, 2, 3, 4, 5 }
    };

    const list<int> expected_result = { 0 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}

TEST( VotingFixture, simple_1can5bal ) {
    const int num_candidates = 1;
    const int num_ballots = 1;
    vector<vector<int>> ballots = {
        { 1 },
        { 1 },
        { 1 },
        { 1 },
        { 1 }
    };

    const list<int> expected_result = { 0 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}

TEST( VotingFixture, simple_5can5bal ) {
    const int num_candidates = 5;
    const int num_ballots = 5;
    vector<vector<int>> ballots = {
        { 1, 2, 3, 4, 5 },
        { 1, 2, 3, 4, 5 },
        { 1, 2, 3, 4, 5 },
        { 1, 2, 3, 4, 5 },
        { 1, 2, 3, 4, 5 }
    };

    const list<int> expected_result = { 0 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}

// -----------
// Elimination
// -----------

TEST( VotingFixture, eliminate_one ) {
    const int num_candidates = 3;
    const int num_ballots = 5;
    vector<vector<int>> ballots = {
        { 1, 2, 3 },
        { 1, 2, 3 },
        { 2, 1, 3 },
        { 2, 1, 3 },
        { 3, 1, 2 }
    };

    const list<int> expected_result = { 0 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}

TEST( VotingFixture, eliminate_repeated ) {
    const int num_candidates = 5;
    const int num_ballots = 8;
    vector<vector<int>> ballots = {
        { 1, 2, 3, 4, 5},
        { 1, 2, 3, 4, 5},
        { 1, 2, 3, 4, 5},
        { 2, 1, 3, 4, 5},
        { 2, 1, 3, 4, 5},
        { 3, 1, 5, 2, 4},
        { 3, 1, 5, 4, 2},
        { 4, 2, 1, 3, 5}
    };

    const list<int> expected_result = { 0 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}


// ---
// Tie
// ---

TEST( VotingFixture, tie_immediate ) {
    const int num_candidates = 5;
    const int num_ballots = 5;
    vector<vector<int>> ballots = {
        { 1, 2, 3, 4, 5},
        { 2, 1, 3, 4, 5},
        { 3, 1, 2, 4, 5},
        { 4, 1, 2, 3, 5},
        { 5, 1, 2, 3, 4},
    };

    const list<int> expected_result = { 0, 1, 2, 3, 4 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}

TEST( VotingFixture, tie_postelimination ) {
    const int num_candidates = 4;
    const int num_ballots = 6;
    vector<vector<int>> ballots = {
        { 1, 2, 3, 4 },
        { 1, 2, 3, 4 },
        { 2, 1, 3, 4 },
        { 2, 1, 3, 4 },
        { 3, 1, 2, 4 },
        { 4, 2, 1, 3 }
    };

    const list<int> expected_result = { 0, 1 };
    ASSERT_EQ( evaluate_case( num_candidates, ballots, num_ballots ), expected_result );
}


