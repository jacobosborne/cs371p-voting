#include "Voting.h"

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

#include <list>
#include <sstream>

//#undef SHOW_DEBUG



list<VoteCase> parse_cases( istream& r, ostream& w ) {
    #ifndef SHOW_DEBUG
    ( void )w; // to make it stop complaining about w being unused when SHOW_DEBUG is undefined
    #endif

    int cases;
    r >> cases;

    #ifdef SHOW_DEBUG
    w << "Cases: " << cases << endl;
    #endif

    list<VoteCase> vcs;

    for ( int case_idx = 0; case_idx < cases; case_idx++ ) {
        VoteCase vc = parse_case( r, w );
        vcs.push_back( vc );
    }

    return vcs;
}

VoteCase parse_case( istream& r, ostream& w ) {
    #ifndef SHOW_DEBUG
    ( void )w; // to make it stop complaining about w being unused when SHOW_DEBUG is undefined
    #endif

    int num_candidates;
    r >> num_candidates;

    #ifdef SHOW_DEBUG
    w << "Num Candidates: " << num_candidates << endl;
    #endif

    // Skip empty line
    string tmp;
    getline( r, tmp );

    // Get the names of the candidates
    vector<string> candidates( num_candidates );

    for ( int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++ ) {
        string tmp;
        getline( r, tmp );
        candidates[candidate_idx] = tmp;

        #ifdef SHOW_DEBUG
        w << "Candidate " << candidate_idx << ": " << candidates[candidate_idx] << endl;
        #endif
    }

    // Get all the lines of ballots
    list<string> tmp_lines;
    int ballot_count = 0;
    getline( r, tmp );
    tmp.erase( remove( tmp.begin(), tmp.end(), '\r' ), tmp.end() );

    while( !tmp.empty() and tmp != "" ) {
        string to_add = tmp;
        tmp_lines.push_back( to_add );

        #ifdef SHOW_DEBUG
        w << "Added line: " << to_add << endl;
        #endif

        ballot_count++;
        getline( r, tmp );
        tmp.erase( remove( tmp.begin(), tmp.end(), '\r' ), tmp.end() );
    }

    // Get the ballots
    vector< vector<int> > ballots( ballot_count );
    ballots.reserve( ballot_count );

    int ballot_idx = 0;

    for ( list<string>::iterator iter = tmp_lines.begin(); iter != tmp_lines.end(); iter++ ) {
        vector<int> new_vector( num_candidates );
        ballots[ballot_idx] = new_vector;

        istringstream line_stream;
        line_stream.str( *iter );

        for ( int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++ ) {
            line_stream >> ballots[ballot_idx][candidate_idx];

            #ifdef SHOW_DEBUG
            w << ballots[ballot_idx][candidate_idx] << " ";
            #endif
        }

        #ifdef SHOW_DEBUG
        w << endl;
        #endif

        ballot_idx++;
    }

    return VoteCase( num_candidates, ballot_count, candidates, ballots );
}

list<int> evaluate_case( int num_candidates, vector<vector<int>>& ballots, int ballot_count ) {
    list<int> result;

    vector<vector<int>::iterator> ballot_choices( ballot_count );

    for ( int i = 0; i < ballot_count; i++ )
        ballot_choices[i] = ballots[i].begin();

    bool in_progress = true;
    vector<bool> eliminated( num_candidates );

    for ( int i = 0; i < num_candidates; i++ )
        eliminated[i] = false;

    while ( in_progress ) {
        vector<int> totals( num_candidates );

        for ( int i = 0; i < num_candidates; i++ )
            totals[i] = 0;

        for ( int ballot_idx = 0; ballot_idx < ballot_count; ballot_idx++ ) {
            vector<int>::iterator current_choice = ballot_choices[ballot_idx];

            bool not_found = true;

            while ( not_found ) {
                if ( eliminated[( *current_choice ) - 1] )
                    current_choice++;

                else {
                    totals[( *current_choice ) - 1]++;
                    not_found = false;
                }
            }

        }

        #ifdef SHOW_DEBUG
        cout << "Totals: ";

        for ( int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++ ) {
            if ( eliminated[candidate_idx] )
                cout << "-1, ";
            else
                cout << totals[candidate_idx] << ", ";
        }

        cout << endl << "Ballot Count: " << ballot_count << endl;
        #endif

        int a_total = -1;

        for ( int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++ ) {
            if ( totals[candidate_idx] > ( ballot_count / 2 ) ) {
                in_progress = false;
                result = { candidate_idx };
                break;
            } else if ( !eliminated[candidate_idx] ) {
                if ( a_total == -1 ) {
                    a_total = totals[candidate_idx];
                    result = { candidate_idx };
                    in_progress = false;
                } else if ( totals[candidate_idx] == a_total ) {
                    result.push_back( candidate_idx );
                    in_progress = false;
                } else {
                    a_total = -2;
                    in_progress = true;
                }
            }
        }

        if ( in_progress ) {
            #ifdef SHOW_DEBUG
            cout << "Failed to find candidate with >50%. Eliminating candidate(s) with lowest number of votes." << endl;
            #endif
            int lowest_idx = -1;

            for ( int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++ ) {
                if ( eliminated[candidate_idx] )
                    continue;

                else if ( lowest_idx == - 1 or totals[candidate_idx] < totals[lowest_idx] )
                    lowest_idx = candidate_idx;
            }

            eliminated[lowest_idx] = true;
            #ifdef SHOW_DEBUG
            cout << "Lowest is " << lowest_idx << " with " << totals[lowest_idx] << " votes. Eliminated." << endl;
            #endif

            for ( int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++ ) {
                if ( eliminated[candidate_idx] )
                    continue;

                else if ( totals[candidate_idx] == totals[lowest_idx] ) {
                    eliminated[candidate_idx] = true;
                    #ifdef SHOW_DEBUG
                    cout << candidate_idx << " also has " << totals[candidate_idx] << " votes. Eliminated." << endl;
                    #endif
                }
            }
        }
    }

    return result;
}

