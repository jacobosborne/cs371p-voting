
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

#include <sstream>

#define MAX_CANDIDATES 20
#define MAX_NAME_LENGTH 80
#define MAX_BALLOTS 1000

//#define SHOW_DEBUG

struct VoteCase {
    int num_candidates;
    string candidates[MAX_CANDIDATES];
    int ballots[MAX_BALLOTS][MAX_CANDIDATES];
};


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int cases;
    cin >> cases;
    #ifdef SHOW_DEBUG
    cout << "Cases: " << cases << endl;
    #endif
    

    for (int case_idx = 0; case_idx < cases; case_idx++) {
        int num_candidates;
        cin >> num_candidates;
        #ifdef SHOW_DEBUG
        cout << "Num Candidates: " << num_candidates << endl;
        #endif
        
        string tmp;
        getline(cin, tmp);
        
        string candidates[MAX_CANDIDATES];
        for (int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++) {
            getline(cin, candidates[candidate_idx]);
            #ifdef SHOW_DEBUG
            cout << "Candidate " << candidate_idx << ": " << candidates[candidate_idx] << endl;
            #endif
        }
        
        int ballots[MAX_BALLOTS][MAX_CANDIDATES];
        
        string current_line;
        getline(cin, current_line);
        int ballot_idx = 0;
        while (!current_line.empty()) {
            istringstream line_stream;
            line_stream.str(current_line);
            for (int entry_idx = 0; entry_idx < num_candidates; entry_idx++) {
                line_stream >> ballots[ballot_idx][entry_idx];
                #ifdef SHOW_DEBUG
                cout << ballots[ballot_idx][entry_idx] << " ";
                #endif
            }
            #ifdef SHOW_DEBUG
            cout << endl;
            #endif
            
            ballot_idx++;
            getline(cin, current_line);
        }
        
        int num_ballots = ballot_idx;
        #ifdef SHOW_DEBUG
        cout << "Num Ballots: " << num_ballots << endl;
        #endif
        
        string result;
        bool in_progress = true;
        bool eliminated[MAX_CANDIDATES];
        for (int i = 0; i < MAX_CANDIDATES; i++) {
            eliminated[i] = false;
        }
        while (in_progress) {
            int totals[MAX_CANDIDATES];
            for (int i = 0; i < MAX_CANDIDATES; i++) {
                totals[i] = 0;
                #ifdef SHOW_DEBUG
                if (eliminated[i]) {
                    totals[i] = -1;
                }
                #endif
            }
            for (int ballot_idx = 0; ballot_idx < num_ballots; ballot_idx++) {
                bool not_found = true;
                int current_choice = 1;
                while(not_found) {
                    int candidate_idx = distance( ballots[ballot_idx], find(ballots[ballot_idx], ballots[ballot_idx] + num_candidates, current_choice) );
                    #ifdef SHOW_DEBUG
                    cout << "Found choice " << current_choice << " at " << candidate_idx << endl;
                    #endif
                    if (eliminated[candidate_idx]) {
                        current_choice++;
                    } else {
                        totals[candidate_idx]++;
                        not_found = false;
                    }
                }
            }
            
            #ifdef SHOW_DEBUG
            cout << "Totals: ";
            for (int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++) {
                cout << totals[candidate_idx] << ", ";
            }
            cout << endl;
            #endif
            
            for (int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++) {
                if (totals[candidate_idx] > num_ballots / 2) {
                    in_progress = false;
                    result = candidates[candidate_idx];
                }
            }
            
            if (in_progress) {
                #ifdef SHOW_DEBUG
                cout << "Failed to find candidate with >50%. Eliminating candidate with lowest number of votes." << endl;
                #endif
                int lowest_idx = 0;
                for (int candidate_idx = 1; candidate_idx < num_candidates; candidate_idx++) {
                    if (eliminated[candidate_idx]) {
                        continue;
                    } else if (totals[candidate_idx] < totals[lowest_idx]) {
                        lowest_idx = candidate_idx;
                    }
                }
                eliminated[lowest_idx] = true;
                #ifdef SHOW_DEBUG
                cout << "Lowest is " << lowest_idx << ". Eliminated." << endl;
                #endif
            }
        }
        
        cout << result << endl;
    }
    
    return 0;
}
