#include <iostream>
#include <sstream>
#include <vector>
#include <stdlib.h>

using namespace std;


int main() {
    int num_cases = 15;
    cout << num_cases << endl << endl;

    for (int case_idx = 0; case_idx < num_cases; case_idx++) {
        int num_candidates = rand() % 20 + 1;
        cout << num_candidates << endl;
        
        vector<int> ballot_template(num_candidates);
        for (int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++) {
            cout << "Candidate #" << candidate_idx << endl;
            ballot_template[candidate_idx] = candidate_idx + 1;
        }

        int num_ballots = rand() % 100 + 1;
        for (int ballot_idx = 0; ballot_idx < num_ballots; ballot_idx++) {
            for (int end = num_candidates; end > 0; end--) {
                int choice = rand() % end;
                cout << ballot_template[choice];
                if (end != 1) {
                    cout << " ";
                }

                int tmp = ballot_template[choice];
                ballot_template[choice ] = ballot_template[end - 1];
                ballot_template[end - 1] = tmp; 
            }
            
            cout << endl;
        }

        cout << endl;
    }


    return 0;
}





