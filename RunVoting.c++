#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

#include <sstream>
#include <list>

#include "RunVoting.h"
#include "Voting.h"



int main() {

    list<VoteCase> vcs = parse_cases( cin, cout );

    for ( list<VoteCase>::iterator iter = vcs.begin(); iter != vcs.end(); iter++ ) {
        list<int> winning_idxs = evaluate_case( iter->num_candidates, iter->ballots, iter->ballot_count );

        for ( list<int>::iterator idx_iter = winning_idxs.begin(); idx_iter != winning_idxs.end(); idx_iter++ )
            cout << iter->candidates[ *idx_iter ] << endl;

        if ( next( iter ) != vcs.end() )
            cout << endl;
    }

    /*
        int cases;
        cin >> cases;

        #ifdef SHOW_DEBUG
        cout << "Cases: " << cases << endl;
        #endif

        for (int case_idx = 0; case_idx < cases; case_idx++) {
            //VoteCase vote_case;

            int num_candidates;
            // Get the number of candidates for the case from the input
            cin >> num_candidates;

            #ifdef SHOW_DEBUG
            cout << "Num Candidates: " << num_candidates << endl;
            #endif

            // Skip empty line
            string tmp;
            getline(cin, tmp);

            // Get the names of the candidates
            vector<string> candidates(num_candidates);
            //vote_case.candidates.reserve(vote_case.num_candidates);
            for (int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++) {
                string tmp;
                getline(cin, tmp);
                candidates[candidate_idx] = tmp;

                #ifdef SHOW_DEBUG
                cout << "Candidate " << candidate_idx << ": " << candidates[candidate_idx] << endl;
                #endif
            }

            // Get all the lines of ballots
            list<string> tmp_lines;
            int ballot_count = 0;
            getline(cin, tmp);
            tmp.erase(remove(tmp.begin(), tmp.end(), '\r'), tmp.end());
            while (!tmp.empty() and tmp != "") {
                #ifdef SHOW_DEBUG
                cout << "Add line: " << tmp << endl;
                #endif
                string to_add = tmp;
                tmp_lines.push_back(to_add);
                ballot_count++;
                getline(cin, tmp);
                tmp.erase(remove(tmp.begin(), tmp.end(), '\r'), tmp.end());
            }

            // Get the ballots
            vector<vector<int>> ballots;
            ballots.reserve(ballot_count);
            int ballot_idx = 0;
            for (list<string>::iterator iter = tmp_lines.begin(); iter != tmp_lines.end(); iter++) {
                vector<int> new_vector(num_candidates);
                ballots.push_back( new_vector );
                istringstream line_stream;

                line_stream.str(*iter);

                for (int candidate_idx = 0; candidate_idx < num_candidates; candidate_idx++) {
                    line_stream >> ballots[ballot_idx][candidate_idx];
                    #ifdef SHOW_DEBUG
                    cout << ballots[ballot_idx][candidate_idx] << " ";
                    #endif
                }
                #ifdef SHOW_DEBUG
                cout << endl;
                #endif

                ballot_idx++;
            }

            list<int> winning_idxs = evaluate_case(num_candidates, ballots, ballot_count);
            for (list<int>::iterator iter = winning_idxs.begin(); iter != winning_idxs.end(); iter++) {
                cout << candidates[ *iter ] << endl;
            }
            if (case_idx != cases - 1) {
                cout << endl;
            }
            //string winner = candidates[ evaluate_case(num_candidates, ballots, ballot_count) ];
            //cout << winner << endl << endl;
        }

        return 0;
    */
}
